<?php 
class CoursesController extends AppController{

public $helpers =array('Html','Form');//para que ayude a crear formulario (crear, editar, eliminar)
public $components =array('Session');//componentes muy importantes (para manejo de session)

	public function index(){
	$this->Course->recursive=0;//recursiva, cantidad de nivels que quiero leer
        $this->set('cursos', $this->paginate());
	
	//para un solo campo	
	//$this->set('Comentarios', $this->Post->findById(2)); 

	}

	public function add(){
	if($this->request->is('post')): //si recibe el formulario en post
		if($this->Course->save($this->request->data)){ //si se pueden guardar los datos
			$this->Session->setFlash('CURSO GUARDADO'); //mensaje luego de salvar
			$this->redirect(array('action' => 'index'));
		}
	endif;
        $teachers= $this->Course->Teacher->find('list');//para relacionar las tablas
        $students= $this->Course->Student->find('list');
        $this->set(compact('teachers','students'));

	}

	public function edit($id = null){
	//le coloco a mi modelo poste el id
	$this->Course->id=$id;
		if($this->request->is('get')){//va a mostrar el formulario lleno
			$this->request->data=$this->Course->read();	
		}else{ //no es get, estonces se quiere guardar
			//print_r($this->request->data);exit;
			if($this->Course->save($this->request->data)){
			//$this->request->data['Post']['title'];
			$this->Session->setFlash('CURSO MODIFICADO');
			$this->redirect(array('action' =>'index'));
			}else{
			$this->Session->setFlash('NO SE PUDO GUARDAR');
			}

		}
	}

	public function delete($id){
	if($this->request->is('get'))://SI ENVIAN POR URL DATA -> NEGAR
		throw new MethodNotAllowedException();
	else:
	if($this->Course->delete($id)):
		$this->Session->setFlash('CURSO ELMINADO');
		$this->redirect(array('action' =>'index'));
	endif;
		
	endif;



	}
}
