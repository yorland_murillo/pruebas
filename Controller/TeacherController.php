<?php 
class TeacherController extends AppController{

public $helpers =array('Html','Form');//para que ayude a crear formulario (crear, editar, eliminar)
public $components =array('Session');//componentes muy importantes (para manejo de session)
public $paginate =array('limit' => 2,
                        'order' => array('Teacher_id' =>'id'));
	public function index(){
	$this->Teacher->recursive =0;
	//el set nos permite colocar algo para que lo usen las listas
	$teachers= $this->paginate();
        
	if($this->request->is('requested'))://si me llega una peticion
            return $teachers;
        else:
            $this->set('profesores', $this->paginate());//<-- Para el paginado y filtro
        endif;
	//para un solo campo	
	//$this->set('Comentarios', $this->Post->findById(2)); 

	}

	public function add(){
	if($this->request->is('post')): //si recibe el formulario en post
		if($this->Teacher->save($this->request->data)){ //si se pueden guardar los datos
			$this->Session->setFlash('PROFESOR GUARDADO'); //mensaje luego de salvar
			$this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash('ERROR AL GUARDAR'); //NO SE GUARDO
			$this->redirect(array('action' => 'index'));
		}
		
	endif;

	}

	public function edit($id = null){
	//le coloco a mi modelo poste el id
	$this->Teacher->id=$id;
		if($this->request->is('get')){//va a mostrar el formulario lleno
			$this->request->data=$this->Teacher->read();	
		}else{ //no es get, estonces se quiere guardar
			//print_r($this->request->data);exit;
			if($this->Teacher->save($this->request->data)){
			//$this->request->data['Post']['title'];
			$this->Session->setFlash('PROFESOR MODIFICADO');
			$this->redirect(array('action' =>'index'));
			}else{
			$this->Session->setFlash('NO SE PUDO GUARDAR');
			}

		}
	}

	public function delete($id){
	if($this->request->is('get')):
		throw new MethodNotAllowedException();
	else:
	if($this->Teacher->delete($id)):
		$this->Session->setFlash('PROFESOR ELMINADO');
		$this->redirect(array('action' =>'index'));
	endif;
		
	endif;



	}
        
        public function view($id = null){
            $this->Teacher->id = $id;
            //para que muestre solamente un dato por id read
            $this->set('teacher',$this->Teacher->read(null,$id));
            //allowedAction para colocar validaciones o restringir usuarios 
            
        }
}
