<?php 
class StudentsController extends AppController{

public $helpers =array('Html','Form');//para que ayude a crear formulario (crear, editar, eliminar)
public $components =array('Session');//componentes muy importantes (para manejo de session)

	public function index(){
	$params= array('order'=>'name asc');	
	//el set nos permite colocar algo para que lo usen las listas
	$this->set('estudiante', $this->Student->find('all',$params));//lo traigo del modelo Post
	
	//para un solo campo	
	//$this->set('Comentarios', $this->Post->findById(2)); 

	}

	public function add(){
	if($this->request->is('post')): //si recibe el formulario en post
		if($this->Student->save($this->request->data)){ //si se pueden guardar los datos
			$this->Session->setFlash('COMENTARIO GUARDADO'); //mensaje luego de salvar
			$this->redirect(array('action' => 'index'));
		}else{
			$this->Session->setFlash('ERROR AL GUARDAR'); //NO SE GUARDO
			$this->redirect(array('action' => 'index'));
		}
		
	endif;

	}

	public function edit($id = null){
	//le coloco a mi modelo poste el id
	$this->Student->id=$id;
		if($this->request->is('get')){//va a mostrar el formulario lleno
			$this->request->data=$this->Student->read();	
		}else{ //no es get, estonces se quiere guardar
			//print_r($this->request->data);exit;
			if($this->Student->save($this->request->data)){
			//$this->request->data['Post']['title'];
			$this->Session->setFlash('ESTUDIANTE MODIFICADO');
			$this->redirect(array('action' =>'index'));
			}else{
			$this->Session->setFlash('NO SE PUDO GUARDAR');
			}

		}
	}

	public function delete($id){
	if($this->request->is('get')):
		throw new MethodNotAllowedException();
	else:
	if($this->Student->delete($id)):
		$this->Session->setFlash('ESTUDIANTE ELMINADO');
		$this->redirect(array('action' =>'index'));
	endif;
		
	endif;



	}
}
