<?php 
class Student extends AppModel{

    public $validate = array(
        'name' => array(
                    'rule' =>'notEmpty',
                    'message' => 'Debe ingresar un Nombre')
        );
    
//AQUI SE COLOCA LA RELACION ENTRE UN ESTUDIANTE A MUCHOS CURSOS
    public $hasAndBelongsToMany =array(
            'Course' => array(
                'className' => 'Course',
                'joinTable' => 'students_courses',
                'foreignKet' => 'student_id',
                'associationForeignKey' => 'course_id'
            )
        );
    
}
