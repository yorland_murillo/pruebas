<?php 
class Post extends AppModel{
	
public $validate = array(
   'title' => array(
      'valid' => array('rule' => array('alphaNumeric'),
                       'message' => 'Nombre debe ser alfanumérico'),
      'required' => array('rule' => array('minLength', '1'),
                            'message' => 'Nombre obligatorio'))
   );  
}
