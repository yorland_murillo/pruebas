<?php 
class Teacher extends AppModel{

        public $displayField ='name';//title, name--
        // public $displayField ='last_name';//para mostrar dicho campo
        //para las relaciones entre tablas "relationship types"
        //1 profesor tiene muchos cursos
        public $hasMany=array(
                    'Course' => array(
                        'className' => 'Course',
                        'foreignKey' => 'teacher_id',
                        'dependent' => true                        
                    )
            );
        
    public $validate = array(
        'name' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Debe ingresar el Nombre')
        );
}
