<?php 
class Course extends AppModel{

        public $displayField ='name';//title, name--
        // public $displayField ='last_name';//para mostrar dicho campo
        //arreglo asociativo, mis cursos pertenecen a un profesor
        // ESTO ES PARA HACER LOS SELECT??
        public $belongsTo=array(
                    'Teacher' => array(
                        'className' => 'Teacher',
                        'foreignKey' => 'teacher_id'
                    )
            );
        public $belongsTo2=array(
            'Course' => array(
                        'className' => 'Course',
                        'foreignKey' => 'course_id'
                    )
        );
        
    public $validate = array(
        'name' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Debe ingresar el Nombre')
        );
    
    public $hasAndBelongsToMany =array(
        'Student' => array(
            'className' => 'Student',
            'jointable' => 'students_courses',
            'foreignKey' => 'course_id',
            'associationForeignKey' => 'student_id'            
        )
    );
            
}
