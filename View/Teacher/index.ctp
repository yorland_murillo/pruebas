<div class="index">
    <h2>LISTADOS DE PROFESORES</h2>
<?php echo $this->Html->link('Agregar Profesor', array('action'=>'add'))?>

    <table>
	<tr>
		<th><?php echo $this->Paginator->sort('name','NOMBRE'); ?></th>
		<th><?php echo $this->Paginator->sort('last_name','APELLIDO'); ?></th>
                <th><?php echo $this->Paginator->sort('cv'); ?></th>
		</tr>
<?php   
//se usa el nombre del modelo, en este caso Post
foreach ($profesores as $llave=>$valor){

echo "<tr>
		<td>".h($valor['Teacher']['name'])."</td>
		<td>".$valor['Teacher']['last_name']."</td>
                <td>".$valor['Teacher']['cv']."</td>
	<td>".$this->Html->link('Editar',array('action'=> 'edit',$valor['Teacher']['id']))."</td>
	<td>".$this->Form->postLink('Eliminar',array('action'=> 'delete',$valor['Teacher']['id']),array('confirm' => 'Realmente desea eliminar este Estudiante?'))."</td>
</tr>";
}

?>
</table>
    <p>
        <?php echo $this->Paginator->counter(array('format' => 'Pagina {:page} de {:pages} , mostrando'
            . ' {:current} registros de {:count}.'));?>
        
    </p>
    <div class="paging">
        <?php echo $this->Paginator->prev('< anterior ',array(), null, array('class' => 'prev disabled'));?>
        <?php echo $this->Paginator->numbers(array('separator' =>''));?>
        <?php echo $this->Paginator->next('siguiente >',array(),null,array('class' => 'next disabled')); ?>
    </div>
</div>
