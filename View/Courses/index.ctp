<h2>LISTADOS DE CURSOS</h2>
<?php echo $this->Html->link('Agregar Cursos', array('action'=>'add'))?>
<table>
	<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name','NOMBRE'); ?></th>
                <th><?php echo $this->Paginator->sort('description','DESCRIPCION'); ?></th>
                <th><?php echo $this->Paginator->sort('name','PROFESOR'); ?></th>
	</tr>
<?php   
//se usa el nombre del modelo, en este caso Post
foreach ($cursos as $llave=>$valor){

echo "<tr>
		<td>".$valor['Course']['id']."</td>
                <td>".$valor['Course']['name']."</td>
		<td>".$valor['Course']['description']."</td>
                <td>".$this->Html->link($valor['Teacher']['name'],
                       array('controller'=>'teachers',
                           'action'=>'view',
                           $valor['Teacher']['id']))."</td>
</tr>";
}

?>
</table>

