<h2>LISTADOS DE COMENTARIOS EN EL BLOCK</h2>
<?php echo $this->Html->link('Agregar Comentario', array('action'=>'add'))?>
<table>
	<tr>
		<th>Titulo</th>
		<th>Comentario</th>
		<th colspan='2'>Acciones</th>
	</tr>
<?php   
//se usa el nombre del modelo, en este caso Post
foreach ($comentarios as $llave=>$valor){

echo "<tr>
		<td>".$valor['Post']['title']."</td>
		<td>".$valor['Post']['body']."</td>
	<td>".$this->Html->link('Editar',array('action'=> 'edit',$valor['Post']['id']))."</td>
	<td>".$this->Form->postLink('Eliminar',array('action'=> 'delete',$valor['Post']['id']),array('confirm' => 'Realmente desea eliminar este comentario?'))."</td>
</tr>";
}

?>
</table>

