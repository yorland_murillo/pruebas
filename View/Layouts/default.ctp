<?php
$cakeDescription = __d('cake_dev', 'CURSOS');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css(array('reset', 'style','fonts'));
                echo $this->Html->script(array('jquery-1.8.1.min'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	
    <header>
        <h1>DIARIO </h1>
    <nav>
        <ul>
            <li><a href="/pages">HOME</a></li>
            <li><a href="/students">ESTUDIANTES</a></li>
            <li><a href="/teacher">PROFESORES</a></li>
            <li><a href="/courses">CURSOS</a></li>
        </ul> 
    </nav>
    </header>
    <div class="wrapper_all">
        
        <section class="content">
            <section id="render_cake">
        <?php echo $this->Session->flash(); ?>
	<?php echo $this->fetch('content'); ?>
            </section>
            <section id="video"></section>
            <aside id="podcast-rel"></aside>
        </section>
        <section class="content">
            <section id="noticias-principal"></section>
            <aside id="noticias-rel">
                <?php echo $this->element('ultimos');?>
            </aside>
        </section>
    </div>
    
    <footer>
        
        
    </footer>
	<?php echo $this->element('sql_dump'); ?>
</body>

