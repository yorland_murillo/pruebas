<h2>LISTADOS DE ESTUDIANTES</h2>
<?php echo $this->Html->link('Agregar Estudiante', array('action'=>'add'))?>
<table>
	<tr>
		<th>Nombre</th>
		<th>Apellido</th>
		<th colspan='2'>Acciones</th>
	</tr>
<?php   
//se usa el nombre del modelo, en este caso Post
foreach ($estudiante as $llave=>$valor){

echo "<tr>
		<td>".$valor['Student']['name']."</td>
		<td>".$valor['Student']['last_name']."</td>
	<td>".$this->Html->link('Editar',array('action'=> 'edit',$valor['Student']['id']))."</td>
	<td>".$this->Form->postLink('Eliminar',array('action'=> 'delete',$valor['Student']['id']),array('confirm' => 'Realmente desea eliminar este Estudiante?'))."</td>
</tr>";
}

?>
</table>

